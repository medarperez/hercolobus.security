import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import logo from "../../../assets/img/brand/logo.svg";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import { ToastContainer } from "react-toastify";



class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: null,
      user: null,
      token: null,
      login: null
    };
  }

  notify = () => toast("Wow so easy !");

  handleLogin = () => {
    debugger;
    const { user, password } = this.state;
    if (user === undefined || user === null) {
      toast("Campo usuario obligatorio");
      return;
    }
    if (password === undefined || password === null) {
      toast("Campo password obligatorio");
      return;
    }
    const request = {
      userId: user,
      password: password
    };

    //fetch("http://ec2-35-160-113-163.us-west-2.compute.amazonaws.com:5000/api/user/token", {
    fetch("http://localhost:5000/api/Autenticacion/token", {
     //fetch(url + "user/token", {
       headers: {
         Accept: "application/json",
         "Content-Type": "application/json"
       },
       method: "POST",
       body: JSON.stringify({
         usuarioId: request.userId,
         password: request.password
       })
     })
       .then(token => token.json())
       .then(token => {
         if (token === "Invalid grant username and/or password is incorrect") {
           toast("Usuario o contraseña invalida!!");
           sessionStorage.setItem("login", false);
           sessionStorage.setItem("token", null);
           sessionStorage.setItem("userId", null);
           return;
         }
         if (token === "Error") {
           toast("Acceso no autorizado");
           sessionStorage.setItem("login", false);
           sessionStorage.setItem("token", null);
           sessionStorage.setItem("userId", null);

           return;
         }
         this.setState({ token });
         const requestUserInfo = {
           ApplicationId: "React",
           ComputerName: "ComputerName",
           UserId: request.userId,
           Token: token.token,
           Login: true
         };
         this.setState({ login: true });
         this.setState({ token: requestUserInfo.Token });
         sessionStorage.requestUserInfo = JSON.stringify(requestUserInfo);
         sessionStorage.setItem("seguridad", requestUserInfo);
         sessionStorage.setItem("login", true);
         sessionStorage.setItem("token", requestUserInfo.Token);
         sessionStorage.setItem("userId", request.userId);
         sessionStorage.setItem("rolId", token.rolId)
    // window.alert('I counted to infinity once then..');
        toast.success("Autenticado");
        this.props.history.push("/");
       return <Redirect to="/Dashboard" />;
       
       });
  };

  handleUserdOnChange = event => {
    this.setState({ user: event.target.value });
  };

  handlePassOnChange = event => {
    this.setState({ password: event.target.value });
  };

  handleOnkeyUp = e => {
    if (e.keyCode === 13) {
      this.handleLogin();
    }
  };

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h1>Ingreso</h1>
                      <p className="text-muted">autenticacion por usuario</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="Usuario" autoComplete="usuario" onChange={this.handleUserdOnChange} />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Password" autoComplete="password" onChange={this.handlePassOnChange}
                          onKeyUp={this.handleOnkeyUp}/>
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4" onClick={this.handleLogin}>Ingreso</Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">Forgot password?</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                  <CardBody className="text-center">
                    <div>
                      <img src={logo} width="100%" alt="Logo" />
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
        <ToastContainer autoClose={5000}/>
      </div>
    );
  }
}

export default Login;
