import axios from "axios";
import { url } from './index';

export default axios.create({
    baseURL: url,
    headers: {
        "Content-Type": "application/json",
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + sessionStorage.getItem('token'),
        "Access-Control-Allow-Origin": "*"
    }
});