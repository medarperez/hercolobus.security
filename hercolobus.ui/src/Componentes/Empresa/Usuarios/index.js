import React, { useState, useEffect } from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row,
    FormGroup,
    Button
} from "reactstrap";
import UsuarioTable from './usuarioTable';
import UsuarioDetalle from './usuarioDetail';
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from './../../Constantes/Api';
import { ToastContainer, toast } from "react-toastify";



const initialUsuarioState = {
    step: 1,
    editMode: "None",
    currentUsuario: {
        id: "",
        usuarioId: "",
        nombre: "",
        correo: "",
        puestoId: 0,
        rolId: 0,
        puesto: {
            id: "",
            nombre: "",
            descripcion: "",
        },
        rol: {
            id: "",
            name: "",
            description: "",
        },
        nombrePuesto: "",
        nombreRol:"",
        password: "",
    },
};


const heightStyleDiv = {
    height: "600px",
    paddingLeft: '0px',
    paddingRight: '0px'
};

const Usuario = props => {
    const [usuarioState, setUsuarioState] = useState(initialUsuarioState);
    const [usuarios, setUsuarios] = useState([]);
    const [roles, setRoles] = useState([]);
    const [puestos, setPuestos] = useState([]);

    function nextStep(item) {
        const { step } = usuarioState;

        if (item === null || item === undefined) {
            item = usuarioState.currentUsuario;
        }
        setUsuarioState({ ...usuarioState, currentUsuario: item, step: step + 1 });
    }

    function fetchUsuarios(query, step) {
        const index = step === undefined || step === null ? 0 : step;
        //  var rol = sessionStorage.getItem('rolId');
        //  if (rol === "3") {
        //      props.history.push('/dashboard');
        //      return (<Redirect to='/dashboard'></Redirect>);
        //  }
         if (query === null) {
             var url = `Usuario/paged?PageSize=10&PageIndex=${index}`
             API.get(url).then(res => {
                 setUsuarios(res.data);
             }).catch(error => {
                 if (error.message === 'Network Error') {
                     toast("Error de red");
                     props.history.push('/login');
                     return (<Redirect to='/login'></Redirect>);
                 }
                 if (error.response.status === 401) {
                     props.history.push('/login');
                     return (<Redirect to='/login'></Redirect>)
                 }
                 console.log(error.response);
             });
         }
         else {
             var url1 = `Usuario/paged?pageSize=10&pageIndex=${index}&value=${query}`;
             API.get(url1).then(res => {
                 setUsuarios(res.data);
             });
         }
    }

    function fectRols() {
        var url = `Rol`;
        API.get(url)
            .then(res => {
                setRoles(res.data);
            })
            .catch(error => {
                if (error.response.status === 401) {
                    props.history.push("/login");
                    return <Redirect to="/login" />;
                }
            });
    }

    function fectPuestos() {
        var url = `Puesto`;
        API.get(url)
            .then(res => {
                setPuestos(res.data);
            })
            .catch(error => {
                if (error.response.status === 401) {
                    props.history.push("/login");
                    return <Redirect to="/login" />;
                }
            });
    }

    useEffect(() => {
        fetchUsuarios(null);
        fectRols();
        fectPuestos();
    }, []);

    function nextPage(step) {
        fetchUsuarios(null, step);
    }

    function prevPage(step) {
        fetchUsuarios(null, step)
    }

    function prevStep() {
        const { step } = usuarioState;

        setUsuarioState({ ...usuarioState, step: step - 1 });
    }

    function add(nuevoUsuario) {
        if (
            !nuevoUsuario ||
            !nuevoUsuario.usuarioId ||
            !nuevoUsuario.nombre ||
            !nuevoUsuario.correo ||
            nuevoUsuario.puestoId <= 0 ||
            nuevoUsuario.rolId <= 0
        ) {
            toast.warn("Faltan datos para poder crear un registro");
            return;
        }
        const url = `Usuario`;

        API.post(url, nuevoUsuario).then(res => {
            debugger;
            if (res.id === 0) {
                toast.error("Error al intentar agregar registro");
            } else {
                toast.success("registro agregado satisfactoriamente");

                fetchUsuarios(null);
                setUsuarioState(initialUsuarioState);
            }
        }).catch(error => {
            debugger;
            if (error.response.status === 401) {
                console.log(error.response.status);
                props.history.push('/login');
                return (<Redirect to='/login'></Redirect>);
            }
            console.log(error.response);
        });
    }

    function update(usuario) {
        if (
            !usuario ||
            !usuario.nombre ||
            usuario.puestoId <= 0 ||
            usuario.rolId <= 0
        ) {

            toast.warn("Faltan datos para poder crear un registro");
            return;
        }
        const url = `Usuario`;
        API.put(url, usuario).then(res => {
            if (res.id === 0) {
                toast.error("Error al intentar agregar puesto");
            } else {
                toast.success("puesto agregado satisfactoriamente");

                // setUsers([...users.users, userAdded]);
                fetchUsuarios(null);
                setUsuarioState(initialUsuarioState);
            }
        }).catch(error => {
            if (error.response.status === 401) {
                console.log(error.response.status);
                props.history.push('/login');
                return (<Redirect to='/login'></Redirect>);
            }
            console.log(error.response);
        });;
    }

    const editRow = usuario => {
        setUsuarioState({
            ...usuarioState,
            editMode: "Editing",
            currentUsuario: usuario,
            step: 2,
        });
    };

    const disabledRegister = id => {
        var usuario = sessionStorage.getItem("usuarioId");
        const url = `Usuario/disabled`;

        const register = {
            id: id,
            user: usuario,
        }
        API.put(url, register).then(res => {
            if (res.data === null || res.data === undefined) {
                toast.error("error al intentar deshabilitar registro");
                return;
            }
            fetchUsuarios(null);
            setUsuarioState(initialUsuarioState);
            toast.success("Registro deshabilitado satisfactoriamente");
        }).catch(error => {
            if (error.response.status === 401) {
                console.log(error.response.status);
                // this.props.history.push("/login");
                props.history.push('/login');
                return (<Redirect to='/login'></Redirect>);

            }
            console.log(error.response);
        });
    };



    function GetCurrentStepComponent(step) {
        switch (step) {
            case 1:
                return (
                    <CardBody >
                        <FormGroup row>
                            <Col />
                            <Col xs="auto">
                                <Button
                                    type="button"
                                    color="success"
                                    onClick={() => nextStep()}>
                                    <i className="fa fa-new" /> Agregar usuario</Button>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col xs="12">
                                <UsuarioTable fetchUsuarios={fetchUsuarios} usuarios={usuarios} nextPage={nextPage} 
                                prevPage={prevPage} editRow={editRow} disabledRegister={disabledRegister} />
                            </Col>
                        </FormGroup>
                    </CardBody>
                );
            case 2:
                return <UsuarioDetalle currentState={usuarioState} setUsuarioState={setUsuarioState}
                    nextStep={nextStep} prevStep={prevStep} roles={roles} puestos={puestos}
                    add={add} update={update}
                />;
            default:
                return null;
        }
    }

    const userStep = GetCurrentStepComponent(usuarioState.step);

    return (
        <div className="animated fadeIn" >
            <Card >
                <Row>
                    <Col>
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify" /> Puestos
                            </CardHeader>
                            {userStep}
                        </Card>
                    </Col>
                </Row>
            </Card>
            <ToastContainer autoClose={5000} />
        </div>
    );
}
export default Usuario;