import React, { useState, useEffect } from "react";
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row,
    Alert,
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";

const UsuarioDetail = props => {
    const { currentState, setUsuarioState, roles, puestos, add, update } = props;
    const [stateTypeAlert, setTypeAlertState] = useState('info');
    const [stateMessagePasswordValidation, setMessagePasswordValidationState] = useState('Password vacio');
    const [stateVerifiedPassword, setVerifiedPasswordState] = useState("");
    const [stateBoolShowMessage, setBoolShowMessageState] = useState(false);
    const [selectedRol, setSelectedRol] = useState("");
    const [selectedRolId, setSelectedRolId] = useState("");
    const [selectedPuesto, setSelectedPuesto] = useState("");
    const [selectedPuestoId, setSelectedPuestoId] = useState("");
    const [dropdownOpen, setDropdownOpen] = useState(false);
    const [dropdownOpenPuesto, setDropdownOpenPuesto] = useState(false);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setUsuarioState({
            ...currentState,
            currentUsuario: {
                ...currentState.currentUsuario,
                [name]: value
            }
        });
    };

    const handleCleanValues = () => {
        const initialClientState = {
            step: 1,
            editMode: "None",
            currentUsuario: {
                id: "",
                usuarioId: "",
                nombre: "",
                correo: "",
                puestoId: 0,
                rolId: 0,
                puesto: {
                    id: "",
                    nombre: "",
                    descripcion: "",
                },
                rol: {
                    id: "",
                    name: "",
                    description: "",
                },
                nombrePuesto: "",
                nombreRol: "",
                password: "",
            }
        };
        setUsuarioState(initialClientState);
    };

    const handleSaveChanges = () => {
        var usuario = sessionStorage.getItem("usuarioId");
        switch (currentState.editMode) {
            case 'Editing': {
                const usuario = {
                    id: currentState.currentUsuario.id,
                    usuarioId: currentState.currentUsuario.usuarioId,
                    nombre: currentState.currentUsuario.nombre,
                    correo: currentState.currentUsuario.correo,
                    rolId: selectedRolId,
                    puestoId: selectedPuestoId,
                    user: usuario,
                }
               update(usuario);
                break;
            }

            default: {
                const nuevoUsuario = {
                    usuarioId: currentState.currentUsuario.usuarioId,
                    nombre: currentState.currentUsuario.nombre,
                    correo: currentState.currentUsuario.correo,
                    rolId: selectedRolId,
                    puestoId: selectedPuestoId,
                    password: currentState.currentUsuario.password,
                    user: usuario,
                }
                add(nuevoUsuario);
                break;
            }
        };
    }

    const handleVerifiedPassword = event => {
        const { value } = event.target;
        const { password } = currentState.currentUsuario;
        if (value !== password) {
            setBoolShowMessageState(true);
            setTypeAlertState('danger');
            setMessagePasswordValidationState('Contraseña invalida!');
            setVerifiedPasswordState(value);

        }
        else {
            setBoolShowMessageState(true);
            setTypeAlertState('success');
            setMessagePasswordValidationState('Contraseña confirmada!');
            setVerifiedPasswordState(value);
        }
    };

    const changeValueRol = e => {
        setSelectedRol(e.currentTarget.textContent);
        setSelectedRolId(e.currentTarget.getAttribute("id"));
    };

    const changeValuePuesto = e => {
        setSelectedPuesto(e.currentTarget.textContent);
        setSelectedPuestoId(e.currentTarget.getAttribute("id"));
    };

    const toggle = event => {
        setDropdownOpen(!dropdownOpen);
    };

    const togglePuesto = event => {
        setDropdownOpenPuesto(!dropdownOpenPuesto);
    };

    switch (currentState.editMode) {
        case 'Editing': return (

            <div className="animated fadeIn">
                <Card>
                    <CardHeader>
                        <strong> Datos a editar </strong>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Usuario</Label>
                                    <Input
                                        autoFocus
                                        type="text"
                                        name="id"
                                        id="id"
                                        value={currentState.currentUsuario.usuarioId}
                                        onChange={handleInputChange}
                                        readonly
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Nombre</Label>
                                    <Input
                                        autoFocus
                                        type="text"
                                        name="name"
                                        id="name"
                                        readonly
                                        value={currentState.currentUsuario.name}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Correo</Label>
                                    <Input type="text" id="description" name="description"
                                        value={currentState.currentPuesto.description}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Rol">Rol</Label>
                                    <Dropdown isOpen={dropdownOpen} toggle={toggle}>
                                        <DropdownToggle caret>{selectedRol}</DropdownToggle>
                                        <DropdownMenu>
                                            {roles.map(e => {
                                                return (
                                                    <DropdownItem
                                                        id={e.id}
                                                        key={e.id}
                                                        onClick={changeValueRol}
                                                    >
                                                        {e.name}
                                                    </DropdownItem>
                                                );
                                            })}
                                        </DropdownMenu>
                                    </Dropdown>
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="rol">Rol Actual</Label>
                                    <Input type="text" id="rolId" name="rolId"
                                        value={currentState.currentUsuario.nombreRol}
                                        readonly
                                        required/>
                                </FormGroup>
                            </Col>         
                        </Row>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Puesto">Puesto</Label>
                                    <Dropdown isOpen={dropdownOpenPuesto} toggle={togglePuesto}>
                                        <DropdownToggle caret>{selectedPuesto}</DropdownToggle>
                                        <DropdownMenu>
                                            {puestos.map(e => {
                                                return (
                                                    <DropdownItem
                                                        id={e.id}
                                                        key={e.id}
                                                        onClick={changeValuePuesto}
                                                    >
                                                        {e.nombre}
                                                    </DropdownItem>
                                                );
                                            })}
                                        </DropdownMenu>
                                    </Dropdown>
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="rol">Puesto Actual</Label>
                                    <Input type="text" id="nombrePuesto" name="nombrePuesto"
                                        value={currentState.currentUsuario.nombrePuesto}
                                        readonly
                                        required />
                                </FormGroup>
                            </Col>       
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <Button type="submit" size="sm" color="success" onClick={handleSaveChanges}>
                            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
                        <Button type="reset" size="sm" color="danger" onClick={handleCleanValues}>
                            <i className="fa fa-ban" /> Cancelar
          </Button>
                    </CardFooter>
                </Card>
            </div>
        );
        default: return (

            <div className="animated fadeIn">
                <Card>
                    <CardHeader>
                        <strong> Datos de nuevo puesto </strong>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Usuario</Label>
                                    <Input
                                        autoFocus
                                        type="text"
                                        name="usuarioId"
                                        id="usuarioId"
                                        value={currentState.currentUsuario.usuarioId}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Nombre</Label>
                                    <Input
                                        type="text"
                                        name="nombre"
                                        id="nombre"
                                        value={currentState.currentUsuario.nombre}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Correo</Label>
                                    <Input type="text" id="correo" name="correo"
                                        value={currentState.currentUsuario.correo}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="password">Contraseña</Label>
                                    <Input type="password" id="Password" name="password"
                                        value={currentState.currentUsuario.password}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="identification">Verificar contraseña</Label>
                                    <Input type="password" id="password"
                                        value={stateVerifiedPassword}
                                        onChange={handleVerifiedPassword}
                                        required />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <CardBody>
                                        <Alert color={stateTypeAlert} isOpen={stateBoolShowMessage}>
                                            {stateMessagePasswordValidation}
                                        </Alert>
                                    </CardBody>
                                </FormGroup>
                            </Col>
                        </Row>
                         <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Rol">Rol</Label>
                                    <Dropdown isOpen={dropdownOpen} toggle={toggle}>
                                        <DropdownToggle caret>{selectedRol}</DropdownToggle>
                                        <DropdownMenu>
                                            {roles.map(e => {
                                                return (
                                                    <DropdownItem
                                                        id={e.id}
                                                        key={e.id}
                                                        onClick={changeValueRol}
                                                    >
                                                        {e.name}
                                                    </DropdownItem>
                                                );
                                            })}
                                        </DropdownMenu>
                                    </Dropdown>
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Puesto">Puesto</Label>
                                    <Dropdown isOpen={dropdownOpenPuesto} toggle={togglePuesto}>
                                        <DropdownToggle caret>{selectedPuesto}</DropdownToggle>
                                        <DropdownMenu>
                                            {puestos.map(e => {
                                                return (
                                                    <DropdownItem
                                                        id={e.id}
                                                        key={e.id}
                                                        onClick={changeValuePuesto}
                                                    >
                                                        {e.nombre}
                                                    </DropdownItem>
                                                );
                                            })}
                                        </DropdownMenu>
                                    </Dropdown>
                                </FormGroup>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <Button type="submit" size="sm" color="success" onClick={handleSaveChanges}>
                            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
                        <Button type="reset" size="sm" color="danger" onClick={handleCleanValues}>
                            <i className="fa fa-ban" /> Cancelar
          </Button>
                    </CardFooter>
                </Card>
            </div>
        );
    }
}
export default UsuarioDetail;