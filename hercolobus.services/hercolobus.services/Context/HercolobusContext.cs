using hercolobus.services.Context.maps;
using hercolobus.services.Features.Passwords;
using hercolobus.services.Features.Puestos;
using hercolobus.services.Features.Roles;
using hercolobus.services.Features.Usuarios;
using Microsoft.EntityFrameworkCore;

namespace hercolobus.services.Context
{
    public class HercolobusContext : DbContext
    {
        public HercolobusContext(DbContextOptions<HercolobusContext> context) : base(context)
        {

        }


        //public DbSet<User> Usuarios { get; set; }
        //public DbSet<UserPassword> Passwords { get; set; }

        public DbSet<Puesto> Puestos { get; set; }
        public DbSet<Rol> Rols { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Password> Passwords { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PuestoMap());
            modelBuilder.ApplyConfiguration(new RolMap());
            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new PasswordMap());
            //modelBuilder.ApplyConfiguration(new UserPasswordMap());
        }
    }
}