using hercolobus.services.Core;
using hercolobus.services.Features.Usuarios;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace hercolobus.services.Context.maps
{
    public class UsuarioMap : EntityMap<Usuario>
    {
        public override void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.Property(t => t.UsuarioId).HasColumnName("UsuarioId").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Nombre).HasColumnName("Nombre").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Correo).HasColumnName("Correo").IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.RolId).HasColumnName("RolId");
            builder.Property(t => t.PuestoId).HasColumnName("PuestoId");

            builder.HasOne(t => t.Rol).WithMany(t => t.Usuarios).HasForeignKey(x => x.RolId);
            builder.HasOne(t => t.Puesto).WithMany(t => t.Usuarios).HasForeignKey(x => x.PuestoId);

            base.Configure(builder);
        }
    }
}