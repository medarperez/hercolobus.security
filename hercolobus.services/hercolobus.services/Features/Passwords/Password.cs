using System;
using System.ComponentModel.DataAnnotations.Schema;
using hercolobus.services.Core;
using hercolobus.services.Features.Usuarios;

namespace hercolobus.services.Features.Passwords
{
    [Table("Passwords")]
    public class Password : Entity
    {
        public int UserId { get; private set; }
        public string PasswordHash { get; private set; }
        public string PasswordHashConfirmacion { get; private set; }
        public Usuario User { get; set; }

        public void DisablePassword(string user)
        {
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "DisabledPassword";
            ModifiedBy = user;
            TransactionUId = Guid.NewGuid();
            Enabled = false;
        }

        public class Builder
        {
            private readonly Password _userPassword = new Password();

            public Builder WithPasswordHash(string passwordHash)
            {
                _userPassword.PasswordHash = passwordHash;
                return this;
            }
            public Builder WithPassword(int userId, string password)
            {
                _userPassword.PasswordHashConfirmacion = password;
                _userPassword.PasswordHash = password;
                _userPassword.UserId = userId;
                return this;
            }

            public Builder WithAuditFields(string user)
            {
                _userPassword.CrudOperation = "Added";
                _userPassword.TransactionDate = DateTime.Now;
                _userPassword.TransactionType = "NewPassword";
                _userPassword.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _userPassword.TransactionUId = Guid.NewGuid();
                _userPassword.Enabled = true;

                return this;
            }

            public Password Build()
            {
                return _userPassword;
            }
        }

    }
}