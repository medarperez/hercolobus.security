using System;
using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Usuarios
{
    public interface IUsuarioAppService : IDisposable
    {
        UsuarioPagedDto GetPagedUser(PagedGeneralRequest request);
        UsuarioDto CreateUser(UsuarioRequest request);
        UsuarioDto UpdateUser(UsuarioRequest request);
        UsuarioDto Disabled(UsuarioRequest request);
        IEnumerable<Usuario> ObtenerUsuarioPorId(string id);
        Usuario ObtenerUsuarioInfo(int id);
        string DeteleUser(string userId);
    }
}