using System;
using hercolobus.services.Core;
using Microsoft.AspNetCore.Mvc;

namespace hercolobus.services.Features.Usuarios
{
    [Route("api/[controller]")]
    [ApiController]
    // [Authorize]
    public class UsuarioController : ControllerBase
    {
        private readonly IUsuarioAppService _usuarioAppService;
        public UsuarioController(IUsuarioAppService usuarioAppService)
        {
            if (usuarioAppService == null) throw new ArgumentException(nameof(usuarioAppService));

            _usuarioAppService = usuarioAppService;
        }
        [HttpGet]
        [Route("paged")]
        // [Authorize]
        public ActionResult<UsuarioPagedDto> GetPaged([FromQuery]PagedGeneralRequest request)
        {
            return Ok(_usuarioAppService.GetPagedUser(request));
        }

        [HttpPost]
        // [Authorize]
        public ActionResult<UsuarioDto> Post([FromBody]UsuarioRequest request)
        {
            return Ok(_usuarioAppService.CreateUser(request));
        }

        [HttpPut]
        // [Authorize]
        public ActionResult<UsuarioDto> Put([FromBody] UsuarioRequest request)
        {
            return Ok(_usuarioAppService.UpdateUser(request));
        }

        [HttpPut]
        [Route("disabled")]
        public ActionResult<UsuarioDto> Disabled([FromBody] UsuarioRequest request)
        {
            return Ok(_usuarioAppService.Disabled(request));
        }
    }
}