using hercolobus.services.Core;

namespace hercolobus.services.Features.Usuarios
{
    public class TokenInfoResponse : ResponseBase
    {
        public string Token { get; set; }
        public string UserId { get; set; }
        public int RolId { get; set; }
        public string Rol { get; set; }
    }
}