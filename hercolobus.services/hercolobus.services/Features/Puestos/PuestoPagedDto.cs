using System.Collections.Generic;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Puestos
{
    public class PuestoPagedDto : ResponseBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<PuestoDto> Items { get; set; }
    }
}