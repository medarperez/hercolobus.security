using System;

namespace hercolobus.services.Features.Puestos
{
    public class PuestoDomainService : IPuestoDomainService
    {
        public Puesto Create(PuestoRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Nombre)) throw new ArgumentNullException(nameof(request.Nombre));

            Puesto zona = new Puesto.Builder()
            .ConNombre(request.Nombre)
            .ConDescripcion(request.Descripcion)
            .ConAuditFields(request.User)
            .Build();

            return zona;
        }

        public Puesto Update(PuestoRequest request, Puesto _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(request.Nombre,request.Descripcion, request.User);

            return _oldRegister;
        }

        public Puesto Disabled(PuestoRequest request, Puesto _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Disbaled(request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        }
    }
}