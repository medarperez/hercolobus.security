using System;

namespace hercolobus.services.Features.Puestos
{
    public interface IPuestoDomainService : IDisposable
    {
        Puesto Create(PuestoRequest request);
        Puesto Update(PuestoRequest request, Puesto _oldRegister);
        Puesto Disabled(PuestoRequest request, Puesto _oldRegister);
    }
}