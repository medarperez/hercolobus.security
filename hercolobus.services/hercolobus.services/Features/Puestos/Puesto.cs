using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using hercolobus.services.Core;
using hercolobus.services.Features.Usuarios;

namespace hercolobus.services.Features.Puestos
{
    [Table("Puestos")]
    public class Puesto : Entity
    {
        public string Nombre { get; private set; } 
        public string Descripcion { get; private set; }

        public ICollection<Usuario> Usuarios { get; set; }

        public void Update(
            string _name, string _description, string _user)
        {
            Nombre = _name;
            Descripcion = _description;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedProject";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public void Disbaled(string _user)
        {
            CrudOperation = "Disabled";
            TransactionDate = DateTime.Now;
            TransactionType = "DisabledRegister";
            ModifiedBy = _user ?? "Application";
            TransactionUId = Guid.NewGuid();
            Enabled = false;
        }

        public class Builder
        {
            private readonly Puesto _puesto = new Puesto();

            public Builder ConNombre(string name)
            {
                _puesto.Nombre = name;
                return this;
            }

            public Builder ConDescripcion(string description)
            {
                _puesto.Descripcion = description;
                return this;
            }

            public Builder ConAuditFields(string user)
            {
                _puesto.CrudOperation = "Added";
                _puesto.TransactionDate = DateTime.Now;
                _puesto.TransactionType = "NewProject";
                _puesto.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _puesto.TransactionUId = Guid.NewGuid();
                _puesto.Enabled = true;

                return this;
            }

            public Puesto Build()
            {
                return _puesto;
            }
        }
    }
}