using hercolobus.services.Core;

namespace hercolobus.services.Features.Puestos
{
    public class PuestoRequest : RequestBase
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }

    }
}