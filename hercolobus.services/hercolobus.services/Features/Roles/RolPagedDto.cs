using System.Collections.Generic;

namespace hercolobus.services.Features.Roles
{
    public class RolPagedDto
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<RolDto> Items { get; set; }
    }
}