using hercolobus.services.Core;

namespace hercolobus.services.Features.Roles
{
    public class RolRequest : RequestBase
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
    }
}