using System;
using System.Collections.Generic;
using System.Linq;
using hercolobus.services.Context;
using hercolobus.services.Core;

namespace hercolobus.services.Features.Roles
{
    public class RolAppService : IRolAppService
    {
        private HercolobusContext _context;
        private readonly IRolDomainService _rolDomainService;

        public RolAppService(HercolobusContext context, IRolDomainService rolDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (rolDomainService == null) throw new ArgumentException(nameof(rolDomainService));

            _context = context;
            _rolDomainService = rolDomainService;
        }
        public RolDto Create(RolRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newpuesto = _rolDomainService.Create(request);

            _context.Rols.Add(newpuesto);
            _context.SaveChanges();

            return new RolDto
            {
                Name = newpuesto.Name,
                Description = newpuesto.Description,
                Enabled = newpuesto.Enabled
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var puesto = _context.Rols.FirstOrDefault(s => s.Id == id);
            if (puesto == null) throw new Exception("Error al intentar obtener puesto");
            _context.Rols.Remove(puesto);
            _context.SaveChanges();

            return string.Empty;
        }

        public RolPagedDto GetPaged(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Rols.Where(s => s.Enabled == true).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Rols.Where(s => s.Enabled == true)
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new RolPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = lista.Select(s => new RolDto
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Description = s.Description,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
            else
            {

                var count = Convert.ToDecimal(_context.Rols.Where(s => s.Name.Contains(request.Value) && s.Enabled == true).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Rols.Where(s => s.Name.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new RolPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Items = lista.Select(s => new RolDto
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Description = s.Description,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
        }

        public RolDto Update(RolRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldRolInfo = _context.Rols.FirstOrDefault(s => s.Id == request.Id);
            if (oldRolInfo == null) return new RolDto { ValidationErrorMessage = "Error al obtener info de los Rols" };

            var rolUpdate = _rolDomainService.Update(request, oldRolInfo);

            _context.Rols.Update(oldRolInfo);
            _context.SaveChanges();

            return new RolDto
            {
                Id = oldRolInfo.Id,
                Name = oldRolInfo.Name,
                Description = oldRolInfo.Description,
                Enabled = oldRolInfo.Enabled
            };
        }

        public RolDto Disabled(RolRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldUsuarioInfo = _context.Rols.FirstOrDefault(s => s.Id == request.Id);
            if (oldUsuarioInfo == null) return new RolDto { ValidationErrorMessage = "Error al obtener info de puesto" };

            var userUpdate = _rolDomainService.Disabled(request, oldUsuarioInfo);

            _context.Rols.Update(oldUsuarioInfo);
            _context.SaveChanges();

            return new RolDto
            {
                Id = oldUsuarioInfo.Id,
                Name = oldUsuarioInfo.Name,
                Description = oldUsuarioInfo.Description,
                Enabled = oldUsuarioInfo.Enabled
            };
        }

        public IEnumerable<RolDto> GetAllRoles()
        {
            var roles = _context.Rols.Where(s => s.Enabled == true).ToList();

            return roles.Select(t => new RolDto
            {
                Id = t.Id,
                Name = t.Name,
                Description = t.Description
            });
        }
        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_rolDomainService != null) _rolDomainService.Dispose();
        }

 
    }
}