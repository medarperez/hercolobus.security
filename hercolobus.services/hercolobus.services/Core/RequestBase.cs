namespace hercolobus.services.Core
{
    public class RequestBase
    {
        public string User { get; set; }
        public string Role { get; set; }
        public string Transaction { get; set; }
        public int Id { get; set; }
    }
}