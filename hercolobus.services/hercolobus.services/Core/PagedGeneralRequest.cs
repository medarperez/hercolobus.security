namespace hercolobus.services.Core
{
    public class PagedGeneralRequest : RequestBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string Value { get; set; }
    }
}