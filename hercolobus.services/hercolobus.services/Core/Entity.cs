using System;
using System.ComponentModel.DataAnnotations;

namespace hercolobus.services.Core
{
    public abstract class Entity
    {
        [Key]
        public int Id { get; protected set; }
        public DateTime CreationDate { get; protected set; } = DateTime.Now;
        public Guid TransactionUId { get; protected set; }
        public string ModifiedBy { get; protected set; }
        public DateTime TransactionDate { get; protected set; }
        public string TransactionType { get; protected set; }
        public string CrudOperation { get; protected set; }
        public bool Enabled { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; protected set; }
    }
}